from fileio import get_main, get_variable
from filechange import write_main, write_variable_file
from terraManager import TerraformManager as tfm
from outputManager import OutputManager as opt

resources = ["google_compute_instance_group_manager", "google_compute_instance_template"]
count_of = [1,1]

# Name of Company
companyName = "jamesBecker"

# Output Location for Files
outputFileFolder = "output/"

# Resource Location
resourcePath = "resources/"

resource_files = []
variable_files = []

def main():

# Create Managers based on inputs

    # terraformManager manages the data transformations
    terraformManager = tfm(companyName)

    # resourceManager manages the resource files
#    resourceManager  = ResourceManager(resourcePath)

    # outputManager manages the output files
    outputManager    = opt(outputFileFolder)


## Manipulate Main File
    for i in resources:
        resource_files.append(get_main(i))
    for i in range(len(count_of)):
        write_main(resource_files[i],count_of[i],outputManager)

## Write Variable File
    for i in resources:
        variable_files.append(get_variable(i))
    for i in range(len(count_of)):
        print (variable_files[i])
        write_variable_file(variable_files[i],count_of[i],outputManager)



## Write Input file


    outputManager.close_files()
if __name__ == "__main__":
    main()
