# Terraform script builder
This software will be used to build terraform core structure based on the terraform resource templates in the "golden-terraform-resources" repository

This is currently a work in progress. to use, edit the 2 arrays resource_files and count_of.  the first array will add the different resources, the second array tells the program how many to print out.

### run:
**python main.py** from the main folder.

### output:
Three files will be created:

- outputs/outputs.tf
- outputs/variables.tf
- outputs/main.tf

These files will be terraform files to run a module.  All connections and counts will need to be added and this should be used to create a base module structure NOT for a fully fleshed out module.
