resource "google_compute_instance_group_manager" "default_0" {

name = "${var.cloudreach_group_manager_name_0}"
base_instance_name = "${var.cloudreach_group_manager_base_instance_name_0}"
instance_template = "${var.cloudreach_group_manager_instance_template_0}"
version{
name = "${var.cloudreach_group_manager_version_name_0}"
instance_template = "${var.cloudreach_group_manager_version_instance_template_0}"
target_size{
fixed = "${var.cloudreach_group_manager_version_target_size_fixed_number_0}"
percent = "${var.cloudreach_group_manager_version_target_size_percent_0}"
}
}
zone = "${var.cloudreach_group_manager_zone_0}"
description = "${var.cloudreach_group_manager_description_0}"
named_port{
name = "${var.cloudreach_group_manager_named_port_name_0}"
port = "${var.cloudreach_group_manager_named_port_port_0}"
}
project = "${var.cloudreach_project_0}"
update_strategy = "${var.cloudreach_group_manager_update_strategy_0}"
target_size = "${var.cloudreach_group_manager_target_size_0}"
target_pool = "${var.cloudreach_group_manager_target_pool_0}"
wait_for_instance = "${var.cloudreach_group_manager_wait_for_instance_0}"
auto_healing_policies{
health_check = "${var.cloudreach_group_manager_health_check_0}"
initial_delay_sec = "${var.cloudreach_group_manager_health_check_delay_0}"
}
rolling_update_policy{
minimal_action = "${var.cloudreach_group_manager_rolling_minimal_action_0}"
type = "${var.cloudreach_group_manager_rolling_type_0}"
max_surge_fixed = "${var.cloudreach_group_manager_rolling_max_surge_fixed_0}"
max_surge_percent = "${var.cloudreach_group_manager_rolling_max_surge_percent_0}"
max_unavailable_fixed = "${var.cloudreach_group_manager_rolling_max_unavailable_fixed_0}"
max_unavailable_percent = "${var.cloudreach_group_manager_rolling_max_unavailable_percent_0}"
min_ready_sec = "${var.cloudreach_group_manager_rolling_min_ready_sec_0}"
}

}
resource "google_compute_instance_template" "default_0" {


}
