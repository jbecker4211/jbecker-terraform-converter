###############################################
######  Variables for instance group manager
##############################################

#### Required

variable "cloudreach_group_manager_name_0" {

    description = "name of zonal group manager"
    type = "string"
}

variable "cloudreach_group_manager_base_instance_name_0" {

    description = "base instance to use for deployment"
    type = "string"

}

variable "cloudreach_group_manager_instance_template_0" {

    description = "instance template to deploy with"
    type = "string"

}

variable "cloudreach_group_manager_zone_0" {

    description = "zone to deploy to"
    type = "string"

}

variable "cloudreach_project_0" {

    description = "project to associate this resource to"
    type = "string"

}

#### Optional no default
variable "cloudreach_group_manager_version_name_0" {

    description = "version name.  Is required if version is used"
    type = "string"

}

variable "cloudreach_group_manager_version_instance_template_0" {
    
    description = "instance template for version"
    type = "string"

}

variable "cloudreach_group_manager_version_target_size_fixed_number_0" {

    description = "number of instances to keep alive.  conflicts with percent"
    type = "string"

}

variable "cloudreach_group_manager_version_target_size_percent_0" {

    description = "percent of instances to keep alive. conflicts with fixed number"
    type = "string"

}

variable "cloudreach_group_manager_description_0" {

    description = "description of the group manager"
    type = "string"

}

variable "cloudreach_group_manager_named_port_name_0" {

    description = "name of the port"
    type = "string"

}

variable "cloudreach_group_manager_named_port_port_0" {

    description = "port for named port"
    type = "string"

}

variable "cloudreach_group_manager_target_size_0" {

    description = "number of running instances for this managed instance group"
    type = "string"

}

variable "cloudreach_group_manager_target_pool_0" {

    description = "URL of all target pools to which new instances in the group are added."
    type = "string"

}

variable "cloudreach_group_manager_wait_for_instance_0" {

    description = "wait for all instances to be created/updated before returning"
    type = "string"

}

variable "cloudreach_group_manager_rolling_minimal_action_0" {

    description = "minimal action to take on an instance. valid: RESTART, REPLACE"
    type = "string"

}

variable "cloudreach_group_manager_rolling_type_0" {

    description = "type of rolling update. Valid: OPPORTUNISTIC, PROACTIVE"
    type = "string"

}

variable "cloudreach_group_manager_rolling_max_surge_fixed_0" {

    description = "maximum number of istances that can be created above the targetSize during the update process.  Confilcts to max_surge_percent"
    type = "string"

}

variable "cloudreach_group_manager_rolling_max_surge_percent_0" {

    description = "maximum percent to surge while implementing update. conflicts with max_fix above"
    type= "string"

}

variable "cloudreach_group_manager_rolling_max_unavailable_fixed_0" {

    description = "max unavailable instances update. conflicts with max_unavailable_percent below"
    type = "string"

}

variable "cloudreach_group_manager_rolling_max_unavailable_percent_0" {

    description = "max unavailable percent of intances while running update. Conflicts with max_fixed above"
    type = "string"

}

variable "cloudreach_group_manager_rolling_min_ready_sec_0" {

    description = "minimum number of seconds to wait for new instances to become ready [0,3600]"
    type = "string"

}

variable "cloudreach_group_manager_health_check_0" {

    description = "the health check resource that signals autohealing"
    type = "string"

}

variable "cloudreach_group_manager_health_check_delay_0" {

    description = "initial amout of time to delay after deployment to send health check"
    type = "string"

}

#### Optional with defaults

variable "cloudreach_group_manager_update_strategy_0" {

    description = "how to deal with updated instance templates. Options include: NONE, RESTART, ROLLING_UPDATE"
    type = "string"
    default = "RESTART"

}


