variable "cloudreach_shared_vpc_host_project_id" {

    description = "id of host project"
    type = "string"

}

variable "cloudreach_shared_vpc_service_project_id" {

    description = "id of service project"
    type = "string"

}
