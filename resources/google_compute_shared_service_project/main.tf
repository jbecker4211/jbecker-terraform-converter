resource "google_compute_shared_vpc_service_project" "default"{

    host_project = "${var.cloudreach_shared_vpc_host_project_id}"
    service_project = "${var.cloudreach_shared_vpc_service_project_id}"

}
