###############################################
######  Variables for instance group manager
##############################################

#### Required

variable "cloudreach_group_manager_name" {

    description = "name of zonal group manager"
    type = "string"
}

variable "cloudreach_group_manager_base_instance_name" {

    description = "base instance to use for deployment"
    type = "string"

}

variable "cloudreach_group_manager_instance_template"{

    description = "instance template to deploy with"
    type = "string"

}

variable "cloudreach_group_manager_zone"{

    description = "zone to deploy to"
    type = "string"

}

variable "cloudreach_project" {

    description = "project to associate this resource to"
    type = "string"

}

#### Optional no default
variable "cloudreach_group_manager_version_name" {

    description = "version name.  Is required if version is used"
    type = "string"

}

variable "cloudreach_group_manager_version_instance_template" {
    
    description = "instance template for version"
    type = "string"

}

variable "cloudreach_group_manager_version_target_size_fixed_number" {

    description = "number of instances to keep alive.  conflicts with percent"
    type = "string"

}

variable "cloudreach_group_manager_version_target_size_percent"{

    description = "percent of instances to keep alive. conflicts with fixed number"
    type = "string"

}

variable "cloudreach_group_manager_description" {

    description = "description of the group manager"
    type = "string"

}

variable "cloudreach_group_manager_named_port_name" {

    description = "name of the port"
    type = "string"

}

variable "cloudreach_group_manager_named_port_port" {

    description = "port for named port"
    type = "string"

}

variable "cloudreach_group_manager_target_size"{

    description = "number of running instances for this managed instance group"
    type = "string"

}

variable "cloudreach_group_manager_target_pool" {

    description = "URL of all target pools to which new instances in the group are added."
    type = "string"

}

variable "cloudreach_group_manager_wait_for_instance" {

    description = "wait for all instances to be created/updated before returning"
    type = "string"

}

variable "cloudreach_group_manager_rolling_minimal_action" {

    description = "minimal action to take on an instance. valid: RESTART, REPLACE"
    type = "string"

}

variable "cloudreach_group_manager_rolling_type" {

    description = "type of rolling update. Valid: OPPORTUNISTIC, PROACTIVE"
    type = "string"

}

variable "cloudreach_group_manager_rolling_max_surge_fixed"{

    description = "maximum number of istances that can be created above the targetSize during the update process.  Confilcts to max_surge_percent"
    type = "string"

}

variable "cloudreach_group_manager_rolling_max_surge_percent" {

    description = "maximum percent to surge while implementing update. conflicts with max_fix above"
    type= "string"

}

variable "cloudreach_group_manager_rolling_max_unavailable_fixed" {

    description = "max unavailable instances update. conflicts with max_unavailable_percent below"
    type = "string"

}

variable "cloudreach_group_manager_rolling_max_unavailable_percent"{

    description = "max unavailable percent of intances while running update. Conflicts with max_fixed above"
    type = "string"

}

variable "cloudreach_group_manager_rolling_min_ready_sec"{

    description = "minimum number of seconds to wait for new instances to become ready [0,3600]"
    type = "string"

}

variable "cloudreach_group_manager_health_check"{

    description = "the health check resource that signals autohealing"
    type = "string"

}

variable "cloudreach_group_manager_health_check_delay"{

    description = "initial amout of time to delay after deployment to send health check"
    type = "string"

}

#### Optional with defaults

variable "cloudreach_group_manager_update_strategy" {

    description = "how to deal with updated instance templates. Options include: NONE, RESTART, ROLLING_UPDATE"
    type = "string"
    default = "RESTART"

}


