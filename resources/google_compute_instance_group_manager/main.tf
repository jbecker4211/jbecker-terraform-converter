resource "google_compute_instance_group_manager" "default" {

    name = "${var.cloudreach_group_manager_name}"
    base_instance_name = "${var.cloudreach_group_manager_base_instance_name}"
    instance_template = "${var.cloudreach_group_manager_instance_template}"
    version {
        name = "${var.cloudreach_group_manager_version_name}"
        instance_template = "${var.cloudreach_group_manager_version_instance_template}"
        target_size {
            fixed = "${var.cloudreach_group_manager_version_target_size_fixed_number}"
            percent = "${var.cloudreach_group_manager_version_target_size_percent}"
        }
    }
    zone = "${var.cloudreach_group_manager_zone}"
    description = "${var.cloudreach_group_manager_description}"
    named_port {
        name = "${var.cloudreach_group_manager_named_port_name}"
        port = "${var.cloudreach_group_manager_named_port_port}"
    }
    project = "${var.cloudreach_project}"
    update_strategy = "${var.cloudreach_group_manager_update_strategy}"
    target_size = "${var.cloudreach_group_manager_target_size}"
    target_pool = "${var.cloudreach_group_manager_target_pool}"
    wait_for_instance = "${var.cloudreach_group_manager_wait_for_instance}"
    auto_healing_policies {
        health_check = "${var.cloudreach_group_manager_health_check}"
        initial_delay_sec = "${var.cloudreach_group_manager_health_check_delay}"
    }
    rolling_update_policy {
        minimal_action = "${var.cloudreach_group_manager_rolling_minimal_action}"
        type = "${var.cloudreach_group_manager_rolling_type}"
        max_surge_fixed = "${var.cloudreach_group_manager_rolling_max_surge_fixed}"
        max_surge_percent = "${var.cloudreach_group_manager_rolling_max_surge_percent}"
        max_unavailable_fixed = "${var.cloudreach_group_manager_rolling_max_unavailable_fixed}"
    max_unavailable_percent = "${var.cloudreach_group_manager_rolling_max_unavailable_percent}"
        min_ready_sec = "${var.cloudreach_group_manager_rolling_min_ready_sec}"
    }

}
