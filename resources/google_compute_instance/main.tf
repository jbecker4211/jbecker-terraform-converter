resource "google_compute_instance" "default" {
    name = "${var.cloudreach_instance_name}"
    machine_type = "${var.cloudreach_instance_machine_type}"
    zone = "${var.cloudreach_instance_zone}"
    tags = "${var.cloudreach_instance_tags}"

    network_interface {
        network = "${var.cloudreach_instance_network}"
        subnetwork = "${var.cloudreach_instance_subnetwork}"
        subnetwork_project = "${var.cloudreach_instance_subnetwork_project}"
        address = "${var.cloudreach_instance_ip_address}"
        access_config {
            nat_ip = "${var.cloudreach_instance_nat_ip}"
            public_ptr_domain_name = "${var.cloudreach_instance_public_ptr_domain_name}"
            network_tier = "${var.cloudreach_instance_network_tier}"
        }
        alias_ip_range = "${var.cloudreach_instance_alias_ip_ranges}"
    }
    boot_disk{
        auto_delete ="${var.cloudreach_instance_auto_delete}"
        device_name = "${var.cloudreach_instance_device_name}"
        disk_encryption_key_raw =  "${var.cloudreach_instance_disk_encryption_key_raw}"
        initialize_params {
            size = "${var.cloudreach_instance_size}"
            type = "${var.cloudreach_instance_type}"
            image = "${var.cloudreach_instance_image}"
        }
        source = "${var.cloudreach_instance_source}"
    }

    allow_stopping_for_update = "${var.cloudreach_instance_allow_stopping_for_update}"
    attached_disk = "${var.cloudreach_instance_attached_disks}"
    can_ip_forward = "${var.cloudreach_instance_can_ip_forward}"
    create_timeout = "${var.cloudreach_instance_create_timeout}"
    description = "${var.cloudreach_instance_description}"
    deletion_protection = "${var.cloudreach_instance_deletion_protection}"
    guest_accelerator = "${var.cloudreach_instance_guest_accelerator}"
    labels = "${var.cloudreach_instance_labels}"
    metadata = "${var.cloudreach_instance_metadata}"
    metadata_startup_script = "${var.cloudreach_instance_metadata_startup_script}"
    min_cpu_platform = "${var.cloudreach_instance_min_cpu_platform}"
    project = "${var.cloudreach_instance_project}"
    scheduling {
        preemptible = "${var.cloudreach_instance_preemptible}"
        on_host_maintenance = "${var.cloudreach_instance_on_host_maintenance}"
        automatic_restart = "${var.cloudreach_instance_automatic_restart}"
    }
    scratch_disk{
        interface = "${var.cloudreach_instance_scratch_interface}"
    }
    service_account{
        email = "${var.cloudreach_instance_service_account_email}"
        scopes = "${var.cloudreach_instance_service_account_scopes}"
    }
    tags = "${var.cloudreach_instance_tags}"
}
