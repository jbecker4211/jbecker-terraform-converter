output "instance_id" {
    
    value = "google_compute_instance.default.instance_id"

}

output "metadata_fingerprint" {
    
    value = "google_compute_instance.default.metadata_fingerprint"

}

output "self_link" {

    value = "google_compute_instance.default.self_link"

}

output "tags_fingerprint" {

    value = "google_compute_instance.default.tags_fingerprint"

}

output "label_fingerprint" {

    value = "google_compute_instance.default.label_fingerprint"

}

output "cpu_platform" {

    value = "google_compute_instance.default.cpu_platform"

}

output "network_interface.0.address" {

    value = "google_compute_instance.default.network_interface.0.address"

}

output "network_interface.0.access_config.0.assigned_nat_ip" {

    value = "google_compute_instance.default.network_interface.o.access_config.0.assigned_nat_ip"

}

output "attached_disk.0.disk_encryption_key_sha256" {

    value = "google_compute_instance.default.attached_disk.0.disk_encryption_key_sha256"

}

output "boot_disk.disk_encryption_key_sha256" {

    value = "google_compute_instance.default.boot_disk.disk_encryption_key_sha256"

}

output "disk.0.disk_encryption_key_sha256" {

    value = "google_compute_instance.default.disk.0.disk_encryption_key_sha256"

}
