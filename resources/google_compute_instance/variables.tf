##########################################
########### VARIABLES FOR INSTANCE GCP
##########################################

#### REQUIRED
variable "cloudreach_instance_machine_type"{

    description = "type of machine to use::: for custom types custom-VCPUS-MEM_IN_MB"
    type = "string"

}

variable "cloudreach_instance_name"{

    description = "name of instance"
    type = "string"

}

variable "cloudreach_instance_zone"{

    description = "zone to deploy instance to"
    type = "string"

}


#### OPTIONAL BUT NOT DEFAULT

variable "cloudreach_instance_network"{

    description = "network to attach to"
    type = "string"

}

variable "cloudreach_instance_subnetwork" {

    description = "subnetwork to attach to"
    type = "string"

}

variable "cloudreach_instance_subnetwork_project"{

    description = "project to which the subnet belongs"
    type = "string"

}

variable "cloudreach_instance_ip_address"{

    description = "ip of the instance"
    type = "string"

}

variable "cloudreach_instance_nat_ip"{

    description = "nat ip of instance"
    type = "string"

}

variable "cloudreach_instance_public_ptr_domain_name"{

    description = "DNS domain name for the public PTR record"
    type = "string"

}

variable "cloudreach_instance_network_tier"{

    description = "network tier options: PREMIUM or STANDARD"
    type = "string"

}

variable "cloudreach_instance_alias_ip_ranges"{

    description = "aliasing ip range for instance each list element must be a map containing: ip_cidr_range and subnetwork_range_name"
    type = "list"

}

variable "cloudreach_instance_device_name"{

    description = "name of boot disk"
    type = "string"

}

variable "cloudreach_instance_disk_encryption_key_raw"{

    description = "disk encryption key"
    type = "string"

}

variable "cloudreach_instance_size"{

    description = "size of the imagein GB.  if not specified will inherit base image size"
    type = "string"

}

variable "cloudreach_instance_type"{

    description = "GCE disk type.  either pd-standard or pd-ssd"
    type = "string"

}

variable "cloudreach_instance_image"{

    description = "image to use"
    type = "string"

}

variable "cloudreach_instance_source"{

    description = "source disk to boot from"
    type = "string"

}

variable "cloudreach_instance_attached_disks"{

    description = "this must be a list of maps containing: source,device_name,mode,disk_encryption_key_raw"
    type = "list"

}

variable "cloudreach_instance_description"{

    description = "instance description"
    type = "string"

}

variable "cloudreach_instance_guest_accelerator"{

    description = "list of maps for guest accelerators.  must contain: type and count"
    type = "list"

}

variable "cloudreach_instance_labels" {

    description = "list of key value pairs assigned to the instance"
    type = "list"

}

variable "cloudreach_instance_metadata"{

    description = "key/value store for instance metadata"
    type = "list"

}

variable "cloudreach_instance_metadata_startup_script"{

    description = "alternative to using the startup-script metadata key"
    type = "string"

}

variable "cloudreach_instance_min_cpu_platform"{

    description = "minimum cpu platform for instance: Intel Haswell, Intel Skylake"
    type = "string"

}

variable "cloudreach_instance_project" {

    description = "project to associate with"
    type = "string"

}


variable "cloudreach_instance_scratch_interface"{

    description = "either SCSI or NVME"
    type = "string"

}

variable "cloudreach_instance_service_account_email"{

    description = "email for service account"
    type = "string"

}

variable "cloudreach_instance_service_account_scopes"{

    description = "scope of servic account"
    type = "string"

}

variable "cloudreach_instance_tags"{

    description = "tags for instance"
    type = "list"

}

#### OPIONAL WITH DEFAULT

variable "cloudreach_instance_preemptible" {

    description = "true of false for preemptible"
    type = "string"
    default = "false"

}

variable "cloudreach_instance_automatic_restart"{

    description = "restart the instance if it it was terminated by Google"
    type = "string"
    default = "true"

}

variable "cloudreach_instance_on_host_maintenance" {

    description = "either MIGRATE or TERMINATE"
    type = "string"
    default = "MIGRATE"

}

variable "cloudreach_instance_auto_delete"{

    description = "whether the disk will be deleted on instance delete"
    type = "string"
    default = "true"

}

variable "cloudreach_instance_allow_stopping_for_update"{

    description = "allow GCP to stop your instance for updates that require them if false these updates will fail"
    type = "string"
    default = "true"

}

variable "cloudreach_instance_can_ip_forward"{

    description = "allow for ip forwarding"
    type = "string"
    default = "false"

}

variable "cloudreach_instance_create_timeout"{

    description = "time out value for creation"
    type ="string"
    default = "4 minutes"

}

variable "cloudreach_instance_deletion_protection"{

    description = "enable deletion protection"
    type = "string"
    default = "false"

}


