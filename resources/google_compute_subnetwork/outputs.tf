output "gateway_address" {
    value = "${google_compute_subnetwork.default.gateway_address}"
}

output "self_link" {
    value = "${google_compute_subnetwork.default.self_link}"
}
