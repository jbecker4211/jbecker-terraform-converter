###################################################
######### variables for google_compute_cloud_subnet
###################################################
#### REQUIRED
variable "cloudreach_subnet_name" {

    description = "name of compute network"
    type = "string"

}

variable "cloudreach_subnet_network" {

    description = "google compute network to assocaite"
    type = "string"

}

variable "cloudreach_subnet_ip_cidr_range" {

    description = "cidr range for subnet"
    type = "string"

}

#### OPTIONAL BUT NOT DEFAULT
variable "cloudreach_subnet_description" {

    description = "describe network"
    type = "string"

}

variable "cloudreach_subnet_project" {

    description = "what project to associate with  PROJECT ID"
    type = "string"

}

variable "cloudreach_subnet_region" {

    description = "region to deploy the subnet"
    type = "string"

}

#### OPTIONAL
variable "cloudreach_subnet_private_ip_google_access"{

    description = "wheather the vms can access other google services without a public ip added"
    type = "string"
    default = "false"

}

variable "cloudreach_subnet_enable_flow_logs" {

    description = "set to true to enable flow logs for the subnet"
    type = "string"
    default = "false"

}

variable "cloudreach_subnet_secondary_ip_range" {

    description = "an array of configurations for secondary IP ranges for VM instances contained in this subnetwork."
    type = "list"
    
        ## Secondary IP range block supports the following:
        ### range_name -- the name associated with the subnetwork
        ### ip_cidr_range -- the range of the ip addresses belonging to this subnetwork
}
