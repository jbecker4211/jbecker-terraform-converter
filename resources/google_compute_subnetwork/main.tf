
resource "google_compute_subnetwork" "default" {

    name = "${var.cloudreach_subnet_name}"
    ip_cidr_range = "${var.cloudreach_subnet_ip_cidr_range}"
    network = "${var.cloudreach_subnet_network}"
    region = "${var.cloudreach_subnet_region}"
    description = "${var.cloudreach_subnet_description}"
    project = "${var.cloudreach_subnet_project}"
    private_ip_google_access = "${var.cloudreach_subnet_private_ip_google_access}"
    enable_flow_logs = "${var.cloudreach_subnet_enable_flow_logs}"
    secondary_ip_range = "${var.cloudreach_subnet_secondary_ip_range}"
}
