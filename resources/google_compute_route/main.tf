resource "google_compute_route" "default" {

    name = "${var.cloudreach_route_name}"
    dest_range = "${var.cloudreach_destination_range}"
    network = "${var.cloudreach_rule_network}"
    priority = "${var.cloudreach_rule_priority}"
    next_hop_gateway = "${var.cloudreach_rule_next_hop_gateway}"
    next_hop_instance = "${var.cloudreach_route_next_hop_instance}"
    next_hop_instance_zone = "${var.cloudreach_route_next_hop_instance_zone}"
    next_hop_ip = "${var.cloudreach_route_next_hop_ip}"
    next_hop_vpn_tunnel = "${var.cloudreach_route_next_hop_vpn_tunnel}"
    project = "${var.cloudreach_route_project}"
    tags = "${var.cloudreach_route_tag}"

}
