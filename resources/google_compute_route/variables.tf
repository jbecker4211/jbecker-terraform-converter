#########################################
######## VARIABLES FOR ROUTE
#########################################

#### REQUIRED

variable "cloudreach_route_name" {

    description = "name of the route"
    type = "string"

}

variable "cloudreach_destination_range" {

    description = "range of IPs for the routes destination"
    type = "string"

}

variable "cloudreach_rule_network" {

    description = "network to attach the route to"
    type = "string"

}

#### OPTIONAL BUT NOT DEFAULT
variable "cloudreach_rule_next_hop_gateway" {

    description = "internet gateway for route if needed"
    type = "string"

}

variable "cloudreach_route_next_hop_instance" {

    description = "instance to route to for the rule"
    type = "string"

}

variable "cloudreach_route_next_hop_instance_zone" {

    description = "next hop instance zone for rule"
    type = "string"

}

variable "cloudreach_route_next_hop_ip" {

    description = "next hop ip for route"
    type = "string"

}

variable "cloudreach_route_next_hop_vpn_tunnel"{

    description = "vpn next hop"
    type = "string"

}

variable "cloudreach_route_project"{

    description = "project to associate the route to"
    type = "string"

}


#### OPTIONAL WITH DEFAULT
variable "cloudreach_rule_priority"{

    description = "priority of the rule"
    type = "string"

}

variable "cloudreach_route_tag"{

    description = "tags associated with route"
    type = "list"

}
