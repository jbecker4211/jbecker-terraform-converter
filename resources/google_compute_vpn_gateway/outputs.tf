output "self_link" {
    value = "${google_compute_vpn_gateway.default.self_link}"
}

output "creation_timestamp" {
    value = "${google_compute_vpn_gateway.default.creation_timestamp}"
}
