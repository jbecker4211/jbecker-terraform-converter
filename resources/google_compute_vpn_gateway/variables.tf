##############################################
########## VARIABLES FOR VPN GATEWAY
##############################################


#### REQUIRED
variable "cloudreach_vpn_name"{

    description = "name for vpn"
    type = "string"

}

variable "cloudreach_vpn_network"{

    description = "network to attach to"
    type = "string"

}

#### OPTIONAL NO DEFAULT
variable "cloudreach_vpn_description"{

    description = "this is a discription"
    type = "string"

}

variable "cloudreach_vpn_region"{

    description = "region to live in"
    type = "string"

}

variable "cloudreach_vpn_project"{

    description = "project association"
    type = "string"

}
