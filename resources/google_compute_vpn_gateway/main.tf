resource "google_compute_vpn_gateway" "default" {
    name = "${var.cloudreach_vpn_name}"
    network = "${var.cloudreach_vpn_network}"
    description = "${var.cloudreach_vpn_description}"
    region = "${var.cloudreach_vpn_region}"
    project = "${var.cloudreach_vpn_project}"
}
