############################################
####### VARIABLES FOR GKE
############################################

#### REQUIRED
variable "cloudreach_node_pool_cluster" {

    type = "string"
    description = "cluster to attach too"

}

#### OPTIONAL NO DEFAULT

variable "cloudreach_node_pool_zone" {

    type = "string"
    description = "zone to deploy to"

}

variable "cloudreach_node_pool_region" {

    type = "string"
    description = "region to deploy too"

}

variable "cloudreach_node_pool_min_node_count" {

    type = "string"
    description = "min node count for autoscaling policy"

}

variable "cloudreach_node_pool_max_node_count"{

    type = "string"
    description = "max nodes in pool"
}

variable "cloudreach_node_pool_initial_node_count"{

    type = "string"
    description = "initial number of nodes in the pool"

}

variable "cloudreach_node_pool_auto_repair"{

    type = "string"
    description = "if the nodes will be autorepaired or not"

}

variable "cloudreach_node_pool_auto_upgrade"{

    type = "string"
    description = "auto update the nodes"

}

variable "cloudreach_node_pool_name"{

    type = "string"
    description = "name of node pool"

}

variable "cloudreach_node_pool_node_count"{

    type = "string"
    description = "number of nodes in the pool"

}

variable "cloudreach_node_pool_project"{

    type = "string"
    description = "project for node pool"

}

variable "cloudreach_node_pool_version"{

    type = "string"
    description = "version of nodes"

}
###### NODE CONFIGURATION
variable "cloudreach_node_pool_disk_size" {

    type = "string"
    description = "size of attached disk for node"
    default = "100GB"

}

variable "cloudreach_node_pool_guest_accelerator_type" {

    type = "string"
    description = "type of accelerator to associate with the nodes"
    default = "nvidia-tesla-k80"

}

variable "cloudreach_node_pool_guest_accelerator_count"{

    type = "string"
    description = "number of guest accelerators"
    default = "1"

}

variable "cloudreach_node_pool_image_type"{

    type = "string"
    description = "image to use for nodes"

}

variable "cloudreach_node_pool_node_labels"{

    type = "list"
    description = "labels for nodes"

}

variable "cloudreach_node_pool_local_ssd_count"{
    type = "string"
    description = "number of ssds for each node"
    default = "0"

}

variable "cloudreach_node_pool_machine_type"{

    type = "string"
    description = "machine type for k8 nodes"
    default = "n1-standard-1"

}

variable "cloudreach_node_pool_metadata"{

    type = "map"
    description = "metadata for nodes"

}

variable "cloudreach_node_pool_min_cpu_platform"{

    type = "string"
    description = "min cpu for k8"
    default= "Intel Haswell"

}

variable "cloudreach_node_pool_oath_scopes"{

    type = "list"
    description = "access to what systems"
    default = ["https://www.googleapis.com/auth/compute",
          "https://www.googleapis.com/auth/devstorage.read_only",
          "https://www.googleapis.com/auth/logging.write",
          "https://www.googleapis.com/auth/monitoring"]

}

variable "cloudreach_node_pool_preemptible"{

    type = "string"
    description = "if they are pre emptible"
    default = "false"

}

variable "cloudreach_node_pool_node_service_account"{

    type = "string"
    description = "service account for nodes to use"

}

variable "cloudreach_node_pool_node_tags"{

    type = "list"
    description = "tags for nodes"

}

variable "cloudreach_node_pool_node_key"{

    type = "string"
    description = "key for taint"

}

variable "cloudreach_node_pool_node_value"{

    type = "string"
    description = "value for taint"

}

variable "cloudreach_node_pool_node_effect"{

    type = "string"
    description = "effect for taint"

}

variable "cloudreach_node_pool_node_metadata"{

    type = "string"
    description = "either UNSPECIFIED, SECURE, EXPOSE"
    default = "UNSPECIFIED"

}
