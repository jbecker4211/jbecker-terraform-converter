resource "google_container_node_pool" "default" {

    zone = "${var.cloudreach_node_pool_zone}"
    region = "${var.cloudreach_node_pool_region}"
    cluster = "${var.cloudreach_node_pool_cluster}"
    autoscaling {
        min_node_count = "${var.cloudreach_node_pool_min_node_count}"
        max_node_count = "${var.cloudreach_node_pool_max_node_count}"
    }
    initial_node_count = "${var.cloudreach_node_pool_initial_node_count}"
    management {
        auto_repair = "${var.cloudreach_node_pool_auto_repair}"
        auto_upgrade = "${var.cloudreach_node_pool_auto_upgrade}"
    }
    name = "${var.cloudreach_node_pool_name}"
    node_config {
        disk_size_gb = "${var.cloudreach_node_pool_disk_size}"
        guest_accelerator {
            type = "${var.cloudreach_node_pool_guest_accelerator_type}"
            count = "${var.cloudreach_node_pool_guest_accelerator_count}"
        }
        image_type = "${var.cloudreach_node_pool_image_type}"
        labels = "${var.cloudreach_node_pool_node_labels}"
        local_ssd_count = "${var.cloudreach_node_pool_local_ssd_count}"
        machine_type = "${var.cloudreach_node_pool_machine_type}"
        metadata = "${var.cloudreach_node_pool_metadata}"
        min_cpu_platform = "${var.cloudreach_node_pool_min_cpu_platform}"
        oauth_scopes = ["${var.cloudreach_node_pool_oath_scopes}"]
        preemptible = "${var.cloudreach_node_pool_preemptible}"
        service_account = "${var.cloudreach_node_pool_node_service_account}"
        tags = ["${var.cloudreach_node_pool_node_tags}"]
        taint { 
           key = "${var.cloudreach_node_pool_node_key}"
           value = "${var.cloudreach_node_pool_node_value}"
           effect = "${var.cloudreach_node_pool_node_effect}"
        }
         workload_metadata_config {
            node_metadata = "${var.cloudreach_node_pool_node_metadata}"
        }
    }
    node_count = "${var.cloudreach_node_pool_node_count}"
    project = "${var.cloudreach_node_pool_project}"
    version = "${var.cloudreach_node_pool_version}"
}
