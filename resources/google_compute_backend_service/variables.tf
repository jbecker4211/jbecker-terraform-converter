#########################################
#### Variables for google backend service
#########################################


#### REQUIRED
variable "cloudreach_backend_name" {

    type = "string"
    description = "google backend name"

}

variable "cloudreach_backend_health_checks"{

    type = "list"
    description = "list of healthchecks"

}


#### OPTION NOT DEFAULT
variable "cloudreach_backend_custom_request_headers"{

    type = "string"
    description = "headers that the HTTP/S load balncer should add to proxied requests"

}

variable "cloudreach_main_backend_description"{

    type = "string"
    description = "describe the backend"

}

variable "cloudreach_backend_project"{

    type = "string"
    description = "what project to assocaite to"

}

variable "cloudreach_backend_security_policy"{

    type = "string"
    description = "security policy"

}


#### OPTIONAL DEFAULT
variable "cloudreach_backend_connection_draining_timeout_sec"{

    type = "string"
    description = "time where instances drain their queue"
    default ="300"

}

variable "cloudreach_backend_enable_cdn"{

    type = "string"
    description = "enable or disable cdn"
    default = "false"

}

variable "cloudreach_backend_port_name" {
    type = "string"
    description = "name the service that has been added to an instance group"
    default = "http"

}

variable "cloudreach_backend_protocol"{

    type = "string"
    description = "protocol for the balancer"
    default = "HTTP"

}

variable "cloudreach_backend_session_affinity"{

    type = "string"
    description = "stateful load balancer or not"
    default = "NONE"

}

variable "cloudreach_backend_timeout_sec"{

    type = "string"
    description = "timout for backend response"
    default = "30"

}


#### IF CDN-POLICY
variable "cloudreach_cdn_cache_policy_include_host"{

    type = "string"
    description = "include host in caching policy"

}

variable "cloudreach_cdn_include_protocol"{

    type = "string"
    description = "boolean if true cache http and https seperatly"

}

variable "cloudreach_cdn_include_query_string"{

    type = "string"
    description = "if true include query strings for whitelist and blacklist"

}

variable "cloudreach_cdn_query_string_blacklist"{

    type = "string"
    description = "string parameters to exclude in cache keys"

}

variable "cloudreach_cdn_query_string_whitelist"{

    type = "string"
    description = "whitelist query"

}


#### IF BACKEND IS SPECIFIED
variable "cloudreach_backend_group"{

    type = "string"
    description = "group for backend if specified: instance manager for backend groups

}

variable "cloudreach_backend_balancing_mode"{

    type = "string"
    description = "balancing mode for backend if specified"

}

variable "cloudreach_backend_capacity_scaler"{

    type = "string"
    description = "[0,1.0] basically weight based routing"

}

variable "cloudreach_backend_description"{

    type = "string"
    description = "description of the group"

}

variable "cloudreach_backend_group_max_rate"{

    type = "string"
    description = "max requests per second the fgroup can handle"

}

variable "cloudreach_backend_max_rate_per_instance"{

    type = "string"
    description = "max per instance per second"

}

variable "cloudreach_backend_max_connections"{

    type = "string"
    description = "max number of connections for this group"

}

variable "cloudreach_backend_max_connections_per_instance" {

    type ="string"
    description = "max number of connections per instance"

}

variable "cloudreach_backend_max_utilization"{

    type = "string"
    description = "max utalization for the backend"

}


######## IF IAP
variable "cloudreach_backend_oauth2_client_id"{

    type = "string"
    description = "oauth for iap block"

}

variable "cloudreach_backend_oath2_client_secret"{

    type = "string"
    description = "oath client secret for iap block"

}
