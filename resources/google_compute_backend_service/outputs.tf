output "fingerprint"{
    value = "${google_compute_backend_service.default.fingerprint}"
}

output "self_link"{
    value = "${google_compute_backend_service.default.self_link}"
}
