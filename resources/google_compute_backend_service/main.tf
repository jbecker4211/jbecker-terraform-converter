resource "google_compute_backend_service" "default" {

    name = "${var.cloudreach_backend_name}"
    health_checks = ["${var.cloudreach_backend_health_checks}"]
    backend = {

        group = "${var.cloudreach_backend_group}"
        balancing_mode = "${var.cloudreach_backend_balancing_mode}"
        capacity_scalar = "${var.cloudreach_backend_capacity_scalar}"
        description = "${var.cloudreach_backend_description}"
        max_rate = "${var.cloudreach_backend_group_max_rate}"
        max_rate_per_instance = "${var.cloudreach_backend_max_rate_per_instance}"
        max_connections = "${var.cloudreach_backend_max_connections}"
        max_connections_per_instance = "${var.cloudreach_backend_max_connections_per_instance}"
        max_utalization = "${var.cloudreach_backend_max_utalization}"

}

    iap = {

        oauth2_client_id = "${var.cloudreach_backend_oauth2_client_id}"
        oauth2_client_secret = "${var.cloudreach_backend_oauth2_client_secret}"

}
    cdn_policy{
        cache_key_policy{

            include_host = "${var.cloudreach_cdn_cache_policy_include_host}"
            include_protocol = "${var.cloudreach_cdn_include_protocol}"
            include_query_string = "${var.cloudreach_cdn_include_query_string}"
            query_string_blacklist = "${var.cloudreach_cdn_query_string_blacklist}"
            query_string_whitelist = "${var.cloudreach_cdn_query_string_whitelist}"

        }
    }

    connection_draining_timeout_sec = "${var.cloudreach_backend_connection_draining_timeout_sec}"
    custom_request_headers = "${var.cloudreach_backend_custom_request_headers}"
    description = "${var.cloudreach_main_backend_description}"
    enable_cdn = "${var.cloudreach_backend_enable_cdn}"
    port_name = "${var.cloudreach_backend_port_name}"
    project = "${var.cloudreach_backend_project}"
    protocol = "${var.cloudreach_backend_protocol}"
    security_policy = "${var.cloudreach_backend_security_policy}"
    session_affinity = "${var.cloudreach_backend_session_affinity}"
    timeout_sec = "${var.cloudreach_backend_timeout_sec}"

}
