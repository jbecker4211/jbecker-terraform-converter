################################################
###########  variables for GCP firewall
################################################

#### REQUIRED
variable "cloudreach_firewall_name" {

    description = "name of the firewall object"
    type = "string"

}

variable "cloudreach_network" {

    description = "name of network to attach the firewall to"
    type = "string"

}

variable "cloudreach_firewall_allowProtocol" {

    description = "protocol to allow for network"
    type = "string"

}

variable "cloudreach_firewall_allowPort" {

    description = "ports to open"
    type = "list"

}


#### OPTIONAL BUT NOT DEFAULT
variable "cloudreach_firewall_denyProtocol" {

    description = "protocol to deny"
    type = "string"

}

variable "cloudreach_firewall_denyPort" {

    description = "port to close on firewall"
    type = "list"

}

variable "cloudreach_firewall_description"{

    description = "description of firewall"
    type = "string"

}

variable "cloudreach_project" {

    description = "project to associate with"
    type = "string"

}

variable "cloudreach_firewall_source_ranges" {

    description = "CIDR ranges for the firewall"
    type = "list"

}

#### OPTIONAL DEFAULT
variable "cloudreach_firewall_disabled" {

    description = "disable the firewall"
    type = "string"
    default = "false"

}

variable "cloudreach_firewall_priority" {

    description = "firewall priority 0-65535"
    type = "string"
    default = "1000"

}

variable "cloudreach_firewall_source_tags" {

    description = "source tags for firewall"
    type = "list"

}

variable "cloudreach_firewall_target_tags" {

    description = "target tags for the firewall"
    type = "list"

}

variable "cloudreach_firewall_direction" {

    description = "which way to apply firewall rules"
    type = "string"
    default = "INGRESS"

}

variable "cloudreach_firewall_destination_ranges" {

    description = "CIDR ranges for destinations that this firewall applies to"
    type = "list"

}

variable "cloudreach_firewall_source_service_accounts"{

    description = "list of source service accounts that the firewall will apply to"
    type = "list"

}

variable "cloudreach_firewall_target_service_accounts" {

    description = "list of target service accounts for firewall access"
    type = "list"

}
