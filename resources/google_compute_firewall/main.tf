resource "google_compute_firewall" "default" {

    name = "${var.cloudreach_firewall_name}"
    network = "${var.cloudreach_network}"

    allow {
        protocol = "${var.cloudreach_firewall_allowProtocol}"
        ports = "${var.cloudreach_firewall_allowPort}"
    }

    deny {
        protocol = "${var.cloudreach_firewall_denyProtocol}"
        ports = "${var.cloudreach_firewall_denyPort}"
    }

    description = "${var.cloudreach_firewall_description}"
    disabled = "${var.cloudreach_firewall_disabled}"
    project = "${var.cloudreach_project}"
    priority = "${var.cloudreach_firewall_priority}"
    source_ranges = "${var.cloudreach_firewall_source_ranges}"
    source_tags = "${var.cloudreach_firewall_source_tags}"
    target_tags = "${var.cloudreach_firewall_target_tags}"
    direction = "${var.cloudreach_firewall_direction}"
    destination_ranges = "${var.cloudreach_firewall_destination_ranges}"
    source_service_accounts = "${var.cloudreach_firewall_source_service_accounts}"
    target_service_accounts = "${var.cloudreach_firewall_target_service_accounts}"

}
