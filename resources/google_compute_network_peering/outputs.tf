output "state_peer_1" {

    value = "${google_compute_network_peering.peer1.state}"

}

output "state_peer_2" {

    value = "${google_compute_network_peering.peer2.state}"

}

output "state_details_peer_1" {

    value = "${google_compute_network_peering.peer1.state_details}"

}

output "state_details_peer_2" {

    value = "${google_compute_network_peering.peer2.state_details}"

}
