resource "google_compute_network_peering" "peer1"{

    name = "${var.cloudreach_vpc_peer_name}"
    network = "${var.cloudreach_vpc_peer_network}"
    peer_network = "${var.cloudreach_vpc_peer_peer}"

    auto_create_routes = "${var.cloudreach_vpc_peer_auto_create_routes}"

}

resource "google_compute_network_peering" "peer2" {

    name = "${var.cloudreach_vpc_peer_name2}"
    network = "${var.cloudreach_vpc_peer_network2}"
    peer_network = "${var.cloudreach_vpc_peer_peer2}"

    auto_create_routes = "${var.cloudreach_vpc_peer_auto_create_routes2}"

}
