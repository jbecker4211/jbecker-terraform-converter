######################################################
########### Variables for VPC peer
#####################################################
# Peering is bidirectional so you need both
#####################################################

#### Required

variable "cloudreach_vpc_peer_name" {

    description = "name of peering"
    type = "string"

}

variable "cloudreach_vpc_peer_network" {

    description = "self_link to network to peer from"
    type = "string"

}

variable "cloudreach_vpc_peer_peer" {

    description = "self_link of network to peer to"
    type = "string"

}

variable "cloudreach_vpc_peer_name2" {

    description = "name of peering"
    type = "string"

}

variable "cloudreach_vpc_peer_network2" {

    description = "self_link to network to peer from"
    type = "string"

}

variable "cloudreach_vpc_peer_peer2" {

    description = "self_link of network to peer to"
    type = "string"

}

#### Optional with default

variable "cloudreach_vpc_peer_auto_create_routes" {

    description = "true to auto generate routes between vpc"
    type = "string"
    default = "false"

}

variable "cloudreach_vpc_peer_auto_create_routes2" {

    description = "true to auto generate routes between vpc"
    type = "string"
    default = "false"

}
