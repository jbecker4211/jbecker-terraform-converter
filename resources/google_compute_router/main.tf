resource "google_compute_router" "default" {

    name = "${var.cloudreach_router_name}"
    network = "${var.cloudreach_router_network}"
    
    bgp {
         asn = "${var.cloudreach_router_asn}"
    }

    description = "${var.cloudreach_router_description}"
    project = "${var.cloudreach_project}"
    region = "${var.cloudreach_router_region}"

}
