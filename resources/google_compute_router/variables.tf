##############################################
########  cloud router variables
#############################################


#### Required

variable "cloudreach_router_name" {

    description = "name of router"
    type = "string"

}

variable "cloudreach_router_network" {

    description = "network to attach to router"
    type = "string"

}

variable "cloudreach_router_asn" {

    description = "asn number for bgp"
    type = "string"

}

#### Optional

variable "cloudreach_router_description" {

    description = "describe the router"
    type = "string"

}

variable "cloudreach_project" {

    description = "project to associate to"
    type = "string"

}

variable "cloudreach_router_region" {

    description = "region to assocaite with"
    type = "string"

}

