###############################################
########### variables for vpn tunnel
###############################################

#### Required

variable "cloudreach_vpn_tunnel_name" {
    
    description = "tunnel name"
    type = "string"

}

variable "cloudreach_vpn_tunnel_peer_ip" {

    description = "ip to peer to"
    type = "string"

}

variable "cloudreach_vpn_tunnel_shared_secret" {

    description = "shared string between peers"
    type = "string"

}

variable "cloudreach_vpn_tunnel_target_vpn_gateway" {

    description = "vpn gateway to link to self link"
    type = "string"
}

#### Optional

variable "cloudreach_vpn_tunnel_description" {

    description = "description of the tunnel"
    type= "string"

}

variable "cloudreach_vpn_tunnel_local_traffic_selector" {

    description = "select subnets to the peer.  manatory if gateway is attached to custom subnet network"
    type = "list"
}

variable "cloudreach_vpn_tunnel_remote_traffic_selector" {

    description = "remote subnets you can route to"
    type = "list"
}

variable "cloudreach_vpn_tunnel_router" {

    description = "router to use"
    type = "string"

}

variable "cloudreach_project" {

    description = "project to associate to"
    type = "string"

}

variable "cloudreach_vpn_tunnel_region" {

    description = "region for the tunnel"
    type = "string"

}

#### Optional with default

variable "cloudreach_vpn_tunnel_ike_version" {

    description = "ike version default to 2 but can be 1"
    type = "string"
    default = "2"
}
