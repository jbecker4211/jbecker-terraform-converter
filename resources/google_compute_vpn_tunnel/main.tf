resource "google_compute_vpn_tunnel" "default" {

    name = "${var.cloudreach_vpn_tunnel_name}"
    peer_ip = "${var.cloudreach_vpn_tunnel_peer_ip}"
    shared_secret = "${var.cloudreach_vpn_tunnel_shared_secret}"
    target_vpn_gateway = "${var.cloudreach_vpn_tunnel_target_vpn_gateway}"
    description = "${var.cloudreach_vpn_tunnel_description}"
    ike_version = "${var.cloudreach_vpn_tunnel_ike_version}"
    local_traffic_selector = "${var.cloudreach_vpn_tunnel_local_traffic_selector}"
    remote_traffic_selector = "${var.cloudreach_vpn_tunnel_remote_traffic_selector}"
    router = "${var.cloudreach_vpn_tunnel_router}"
    project = "${var.cloudreach_project}"
    region = "${var.cloudreach_vpn_tunnel_region}"

}
