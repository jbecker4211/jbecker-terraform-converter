output "detailed_status" {

    value = "${google_compute_vpn_tunnel.default.detailed_status}"

}

output "self_link" {

    value = "${google_compute_vpn_tunnel.default.self_link}"

}
