resource "google_compute_network" "default" {

    name = "${var.cloudreach_vpc_name}"
    auto_create_subnetworks = "${var.cloudreach_vpc_auto_create_subnetworks}"
    routing_mode = "${var.cloudreach_vpc_routing_mode}"
    description = "${var.cloudreach_vpc_description}"
    project = "${var.cloudreach_vpc_project}"

}
