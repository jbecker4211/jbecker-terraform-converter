###########################################
######### variables for google_compute_cloud
############################################
#### REQUIRED
variable "cloudreach_vpc_name" {

    description = "name of compute network"
    type = "string"

}

#### OPTIONAL BUT NOT DEFAULT
variable "cloudreach_vpc_description" {

    description = "describe network"
    type = "string"

}

variable "cloudreach_vpc_project" {

    description = "what project to associate with  PROJECT ID"
    type = "string"

}

#### OPTIONAL
variable "cloudreach_vpc_auto_create_subnetworks" {

    description = "either create subnet in every region or make custom subnets and attach them true/false"
    type = "string"
    default = "true"

}

variable "cloudreach_vpc_routing_mode" {

    description = "REGIONAL or GLOBAL"
    type = "string"
    default = "GLOBAL"

}
