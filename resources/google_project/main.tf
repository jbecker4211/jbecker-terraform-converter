resource "google_project" "default" {

    name = "${var.cloudreach_project_name}"
    project_id = "${var.cloudreach_project_id}"
    org_id = "${var.cloudreach_project_org_id}"
    folder_id = "${var.cloudreach_project_folder_id}"
    billing_account = "${var.cloudreach_project_billing_account_id}"
    skip_delete = "${var.cloudreach_project_skip_delete}"
    google_project_iam_policy = "${var.cloudreach_project_iam_policy}"
    labels = "${var.cloudreach_project_labels}"
    auto_create_network = "${var.cloudreach_project_auto_create_network}"

    app_engine {
        location_id = "${var.cloudreach_project_app_location_id}"
        auth_domain = "${var.cloudreach_project_app_auth_domain}"
        serving_status = "${var.cloudreach_project_app_serving_status}"

        feature_settings {
            split_health_checks = "${var.cloudreach_project_app_split_health_checks}"
        }
    }
}
