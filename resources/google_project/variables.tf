############################################
######## variables for GCP project
############################################

#### Required

variable "cloudreach_project_name" {

    description = "Name of project"
    type = "string"

}

#### Optional

variable "cloudreach_project_id" {

    description = "id of project"
    type = "string"

}

variable "cloudreach_project_org_id" {

    description = "id of organization to attach to"
    type = "string"

}

variable "cloudreach_project_folder_id" {

    description = "folder id for project"
    type = "string"

}

variable "cloudreach_project_billing_account_id" {

    description = "account to bill to"
    type = "string"

}

variable "cloudreach_project_skip_delete" {

    description = "if set to true, project can be deleted without going through the API"
    type = "string"

}

variable "cloudreach_project_iam_policy" {

    description = "iam policy to assocaite with proiject"
    type = "string"

}

variable "cloudreach_project_labels" {

    description = "set of key/value pairs to label the instance with"
    type = "list"

}

variable "cloudreach_project_app_location_id" {

    description = "REQUIRED if using AppEngine link.  this is the location to serve the app from"
    type = "string"

}

variable "cloudreach_project_app_auth_domain" {

    description = "the domain to authenticate to when using AppEngine User API"
    type = "string"
}

variable "cloudreach_project_app_serving_status" {

    description = "serving status of the app"
    type = "string"

}

variable "cloudreach_project_app_split_health_checks" {

    description = "set to false if using legacy health checks"
    type = "string"

}

#### optional with defaults

variable "cloudreach_project_auto_create_network" {

    description = "create a default network"
    type = "string"
    default = "false"
}
