output "number" {

    value = "google_project.default.project_id"

}

output "app_engine.0.name" {

    value = "google_project.default.app_engine.0.name"

}

output "app_engine.0.url_dispatch_rule" {

    value = "google_project.default.app_engine.0.url_dispatch_rule"

}

output "app_engine.0.code_bucket" {

    value = "google_project.default.app_engine.0.code_bucket"

}

output "app_engine.0.default_hostname" {

    value = "google_project.default.app_engine.0.default_hostname"

}

output "app_engine.0.default_bucket" {

    value = "google_project.default.app_engine.0.default_bucket"

}

output "app_engine.0.gcr_domain" {

    value = "google_project.default.app_engine.0.gcr_domain"

}
