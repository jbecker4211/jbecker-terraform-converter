output "endpoint"  {
    value = "${var.google_container_cluster.default.endpoint}"
}

output "instance_group_urls" {
    value = "${var.google_container_cluster.default.instance_group_urls}"
}

output "maintenance_policy.0.daily_maintenance_window.0.duration" {
    value = "${var.google_container_cluster.default.maintenance_policy.0.daily_maintenance_window.0.duration}"
}

output "master_auth.0.client_certificate" {
    value = "${var.google_container_cluster.default.master_auth.0.client_certificate}"
}

output "master_auth.0.client_key"{
    value = "${var.google_container_cluster.default.master_auth.0.client_key}"
}

output "master_auth.0.cluster_ca_certificate"{
    value = "${var.google_container_cluster.default.master_auth.0.cluster_ca_certificate}"
}

output "master_version" {
    value = "${var.google_container_cluster.default.master_version}"
}
