############################################
####### VARIABLES FOR GKE
############################################

#### REQUIRED
variable "cloudreach_k8_name"{

    type = "string"
    description = "name of the cluster"

}



#### OPTIONAL NO DEFAULT
variable "cloudreach_k8_zone"{

    type = "string"
    description = "zone for deployment"

}

variable "cloudreach_k8_initial_node_count"{

    type = "string"
    description = "number of nodes to deploy initially"

}

variable "cloudreach_k8_additional_zones"{

    type = "list"
    description = "other zones to deploy to"

}

variable "cloudreach_k8_username"{

    type = "string"
    description = "master username"

}

variable "cloudreach_k8_password" {

    type = "string"
    description = "password for master"

}

variable "cloudreach_k8_master_authorized_blocks"{

    type = "string"
    description = "list of cidr block to allow traffic from.  to make cluster private omit line"

}

variable "cloudreach_master_cidr_block"{

    type = "string"
    description = "cidr block to use for master subnet range.  must be a /28 network"

}

variable "cloudreach_min_master_version"{

    type = "string"
    description = "minimum version to run on the master"

}


variable "cloudreach_k8_cluster_ipv4_cidr"{

    type = "string"
    description = "ipv4 cidr for the pod network"

}

variable "cloudreach_k8_description" {

    type = "string"
    description = "cluster description"

}

variable "cloudreach_k8_cluster_secondary_range_name"{

    type = "string"
    description = "name of a secondary IP range that will be used for pod IP addresses"

}

variable "cloudreach_k8_service_secondary_range_name" {

    type = "string"
    description = "secondary IP range that will be used for the service network"

}

variable "cloudreach_k8_network"{

    type = "string"
    description = "vpc to attach to"

}

#### OPTIONAL DEFAULT
variable "cloudreach_k8_logging_service" {

    type = "string"
    description = "logging service to use"
    default = "logging.googleapis.com"

}

variable "cloudreach_k8_monitoring_service"{

    type = "string"
    description = "monitoring service"
    default = "monitoring.googleapis.com"

}

variable "cloudreach_k8_maintenance_window"{

    type = "string"
    description = "time to start maintenance"
    default = "03:00"

}

variable "cloudreach_k8_horizontal_pod_autoscaling"{

    type = "string"
    description = "set to true to disable"
    default = "false"

}

variable "cloudreach_k8_enable_kubernetes_alpha"{

    type = "string"
    description = "enable alpha"
    default = "false"

}

variable "cloudreach_k8_enable_legacy_abac" {

    type = "string"
    description = "enable abac"
    default = "false"

}

variable "cloudreach_k8_http_load_balancing"{

    type = "string"
    description = "set to true to disable"
    default = "false"

}

variable "cloudreach_k8_kubernetes_dashboard"{

    type = "string"
    description = "set to true to disable"
    default = "false"

}

variable "cloudreach_k8_network_policy_config"{

    type = "string"
    description = "this must be enabled to enable network policy for the nodes"
    default = "false"

}

variable "cloudreach_k8_policy_provider"{

    type = "string"
    description = "provider for network policy"
    default = "PROVIDER_UNSPECIFIED"

}

variable "cloudreach_k8_policy_enabled"{

    type = "string"
    description = "enable the network policy"
    default = "false"

}

###### NODE CONFIGURATION
variable "cloudreach_k8_disk_size" {

    type = "string"
    description = "size of attached disk for node"
    default = "100GB"

}

variable "cloudreach_k8_guest_accelerator_type" {

    type = "string"
    description = "type of accelerator to associate with the nodes"
    default = "nvidia-tesla-k80"

}

variable "cloudreach_k8_guest_accelerator_count"{

    type = "string"
    description = "number of guest accelerators"
    default = "1"

}

variable "cloudreach_k8_image_type"{

    type = "string"
    description = "image to use for nodes"

}

variable "cloudreach_k8_node_labels"{

    type = "list"
    description = "labels for nodes"

}

variable "cloudreach_k8_local_ssd_count"{
    type = "string"
    description = "number of ssds for each node"
    default = "0"

}

variable "cloudreach_k8_machine_type"{

    type = "string"
    description = "machine type for k8 nodes"
    default = "n1-standard-1"

}

variable "cloudreach_k8_metadata"{

    type = "map"
    description = "metadata for nodes"

}

variable "cloudreach_k8_min_cpu_platform"{

    type = "string"
    description = "min cpu for k8"
    default= "Intel Haswell"

}

variable "cloudreach_k8_oath_scopes"{

    type = "list"
    description = "access to what systems"
    default = ["https://www.googleapis.com/auth/compute",
          "https://www.googleapis.com/auth/devstorage.read_only",
          "https://www.googleapis.com/auth/logging.write",
          "https://www.googleapis.com/auth/monitoring"]

}

variable "cloudreach_k8_preemptible"{

    type = "string"
    description = "if they are pre emptible"
    default = "false"

}

variable "cloudreach_k8_node_service_account"{

    type = "string"
    description = "service account for nodes to use"

}

variable "cloudreach_k8_node_tags"{

    type = "list"
    description = "tags for nodes"

}

variable "cloudreach_k8_node_key"{

    type = "string"
    description = "key for taint"

}

variable "cloudreach_k8_node_value"{

    type = "string"
    description = "value for taint"

}

variable "cloudreach_k8_node_effect"{

    type = "string"
    description = "effect for taint"

}

variable "cloudreach_k8_node_metadata"{

    type = "string"
    description = "either UNSPECIFIED, SECURE, EXPOSE"
    default = "UNSPECIFIED"

}

variable "cloudreach_k8_node_pool"{

    type = "list"
    description = "list of node pools to associate with the cluster"

}

variable "cloudreach_k8_node_version"{

    type = "string"
    description = "version for nodes"

}

variable "cloudreach_k8_node_security_config"{

    type = "string"
    description = "boolean for enabling the security config"
    default = "false"

}

variable "cloudreach_k8_private_cluster"{

    type = "string"
    description = "is the cluster private or not"
    default = "false"

}

variable "cloudreach_k8_project"{

    type = "string"
    description = "project to associate the cluster to"

}

variable "cloudreach_k8_remove_default_node_pool"{

    type = "string"
    description = "leave or remove the default node pool"

}

variable "cloudreach_k8_subnetwork"{

    type = "string"
    description = "subnet that is used"

}
