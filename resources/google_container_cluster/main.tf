resource "google_container_cluster" "default" {
    name = "${var.cloudreach_k8_name}"
    zone = "${var.cloudreach_k8_zone}"
    initial_node_count = "${var.cloudreach_k8_initial_node_count}"
    additional_zones = ["${var.cloudreach_k8_additional_zones}"]
    addons_config {
        horizontal_pod_autoscaling{
            disabled =  "${var.cloudreach_k8_horizontal_pod_autoscaling}"
        }
        http_load_balancing{ 
            disabled = "${var.cloudreach_k8_http_load_balancing}"
         }
        kubernetes_dashboard{
            disabled = "${var.cloudreach_k8_kubernetes_dashboard}"
         }
        network_policy_config{
            disabled = "${var.cloudreach_k8_network_policy_config}"
        }
    }
    master_auth  {
        username = "${var.cloudreach_k8_username}"
        password = "${var.cloudreach_k8_password}"
    }
    cluster_ipv4_cidr = "${var.cloudreach_k8_cluster_ipv4_cidr}"
    description = "${var.cloudreach_k8_description}"
    enable_kubernetes_alpha = "${var.cloudreach_k8_enable_kubernetes_alpha}"
    enable_legacy_abac = "${var.cloudreach_k8_enable_legacy_abac}"
    ip_allocation_policy{
        cluster_secondary_range_name = "${lookup(google_compute_subnetwork.clustersubnetwork.secondary_ip_range[0], "range_name")}"
        services_secondary_range_name = "${lookup(google_compute_subnetwork.clustersubnetwork.secondary_ip_range[1],"range_name")}"
    }
    logging_service = "${var.cloudreach_k8_logging_service}"
    maintenance_policy{ 
        daily_maintenance_window { 
            start_time = "${var.cloudreach_k8_maintenance_window}"
        }
    }
    master_authorized_networks_config{
        cidr_blocks {
            cidr_block = "${var.cloudreach_k8_master_authorized_blocks}"
        }
    }
    master_ipv4_cidr_block = "${var.cloudreach_master_cidr_block}"
    min_master_version = "${var.cloudreach_min_master_version}"
    monitoring_service = "${var.cloudreach_k8_monitoring_service}"
    network = "${google_compute_network.clustervpc.self_link}"
    network_policy {
        provider = "${var.cloudreach_k8_policy_provider}"
        enabled = "${var.cloudreach_k8_policy_enabled}"
    }
    node_config {
        disk_size_gb = "${var.cloudreach_k8_disk_size}"
        guest_accelerator {
            type = "${var.cloudreach_k8_guest_accelerator_type}"
            count = "${var.cloudreach_k8_guest_accelerator_count}"
        }
        image_type = "${var.cloudreach_k8_image_type}"
        labels = "${var.cloudreach_k8_node_labels}"
        local_ssd_count = "${var.cloudreach_k8_local_ssd_count}"
        machine_type = "${var.cloudreach_k8_machine_type}"
        metadata = "${var.cloudreach_k8_metadata}"
        min_cpu_platform = "${var.cloudreach_k8_min_cpu_platform}"
        oauth_scopes = ["${var.cloudreach_k8_oath_scopes}"]
        preemptible = "${var.cloudreach_k8_preemptible}"
        service_account = "${var.cloudreach_k8_node_service_account}"
        tags = ["${var.cloudreach_k8_node_tags}"]
        taint { 
           key = "${var.cloudreach_k8_node_key}"
           value = "${var.cloudreach_k8_node_value}"
           effect = "${var.cloudreach_k8_node_effect}"
        }
         workload_metadata_config {
            node_metadata = "${var.cloudreach_k8_node_metadata}"
        }
    }
    node_pool = "${var.cloudreach_k8_node_pool}"
    node_version = "${var.cloudreach_k8_node_version}"
    pod_security_policy_config {
        enabled = "${var.cloudreach_k8_node_security_config}"
    }
    private_cluster = "${var.cloudreach_k8_private_cluster}"
    project = "${var.cloudreach_project}"
    remove_default_node_pool = "${var.cloudreach_k8_remove_default_node_pool}"
    subnetwork = "${google_compute_subnetwork.clustersubnetwork.self_link}"
}
