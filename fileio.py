def get_main(input):
    resource = open("./resources/"+input+"/main.tf", "r")
    return resource

def get_variable(input):
    resource = open("./resources/"+input+"/variables.tf","r")
    return resource
