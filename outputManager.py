class OutputManager():

    #Full Path to Output Folder location
    outputFolder = ""
    variableFile = ""
    mainFile     = ""
    invarsFile   = ""
    outputFile   = ""

    def __init__(self,outputFileFolder):
        self.outputFolder = outputFileFolder
        self.mainFile         = self.create_main_file()
        self.variableFile     = self.create_variable_file()
        self.invarsFile       = self.create_invars_file()
        self.outputFile       = self.create_output_file()

    #Get Main for Output
    def create_main_file(self):
        mainFile = open(self.outputFolder+"main.tf","w+")
        return mainFile

    #Get Variable for Output
    def create_variable_file(self):
        variableFile = open(self.outputFolder+"variables.tf","w+")
        return variableFile

    #Get Outputs for Output
    def create_output_file(self):
        outputFile = open(self.outputFolder+"outputs.tf","w+")
        return outputFile

    #Get Inputs for Output
    def create_invars_file(self):
        invarsFile = open(self.outputFolder+"inputs.tfvars","w+")
        return invarsFile

    #Write Main File
    def write_main_file(self,line):
        self.mainFile.write(line)

    #Write Variable File
    def write_variable_file(self,line):
        self.variableFile.write(line)

    def close_files(self):
        self.mainFile.close()
        self.variableFile.close()
        self.invarsFile.close()
        self.outputFile.close()
