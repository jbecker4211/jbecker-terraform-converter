import string, re

def write_main(input,count,outputManager):
    for c in range(count):
        input.seek(0)
        line = input.readline()
        linecount = 0
        while line:
            if "resource" in line:
                lineArray =  line.split(" ")
                for i in range(len(lineArray)):
                    if "default" in lineArray[i]:
                        lineArray[i] = "\"default_" + str(c) + "\""
                outputManager.write_main_file(" ".join(lineArray))
            else:
                newline =  line.translate(None, string.whitespace)
                splitline = newline.split("=")
                for p in range(len(splitline)):
                    if "${var." in splitline[p]:
                        splitline[p] = splitline[p].replace('}\"','_'+str(c)+'}\"')

                outputManager.write_main_file(" = ".join(splitline)+"\n")
            line = input.readline()


def write_variable_file(varFile, count, outputManager):
    for c in range(count):
        varFile.seek(0)
        line = varFile.readline()
        linecount = 0
        while line:
            lineOut = line
            if ("variable" in line) and not ("#" in line):
                varName=""
                lineArray = line.split(" ")
                for i in range(len(lineArray)):
                    if "\"" in lineArray[i]:
                        stripline = lineArray[i].strip("\n")
                        stripline = stripline.strip("{")
                        stripline = stripline.strip("\"")
                        lineArray[i] = "\""+stripline+"_"+str(c)+"\""
                        varName = lineArray[i]
                lineOut = "variable " + varName + " {\n"

            outputManager.write_variable_file(lineOut)
            line = varFile.readline()
